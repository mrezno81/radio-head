module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'drakblue': '#08102D',
      'white': '#ffffff',
      'purple': '#360063',
      'midnight': '#121063',
      'metal': '#565584',
      'tahiti': '#3ab7bf',
      'silver': '#evebff',
      'bubble-gum': '#ff77e9',
      'bermuda': '#78dcca',
      'light-purple': '#A213FB'


    },
    extend: {},
    backgroundImage: {
      'meet-team': "url('../public/assets/meet-team.png')",
      'road-map': "url('../public/assets/road-map.png')",
    },
    fontSize: {
      '41': '41px',
      '30': '30px',
      '21': '21px',
    },
  },
  plugins: [],
}