import './App.css';
import Header from "./components/Header"
import Footer from "./components/Footer"
import Banner from "./components/Banner"
import Team from "./components/Team"
import Trailer from "./components/Trailer"
import Faq from "./components/Faqs/Faq"
import Mint from "./components/Mint"
import Roadmap from "./components/Roadmap"

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <Trailer />
      <Mint />
      <Roadmap />
      <Faq />
      <Team />
      <Footer />
    </div>
  );
}

export default App;
