import React from 'react'

const TeamMemberCard = ({ name, postion, url, children, className }) => {
    return (
        <div className={`relative h-[411px] w-[301.85px] mb-5 xl:mb-0 bg-team-member-card ${className} `}>
            <div className="bg-avatar-stroke absolute top-[50px] left-[165px] translate-x-[-50%] w-[250px] h-[140px] flex justify-center items-center">
                <img className="object-cover" src={`/assets/team/${url}`} alt="avatar" width="100%" height="100%%" />
            </div>

            <div className="absolute bottom-[0px] left-[160px] w-[210px] translate-x-[-50%] text-center ">
                <h2 className=" text-30 text-lg mb-1 text-white">{name}</h2>
                <h3 className=" font-normal text-lg mb-1 text-white">{postion}</h3>
                <span className="text-[13px] font-thin text-white" >{children}</span>
            </div>
        </div>
    );
};

export default function Team() {
    return (
        <>
            <section className="relative mx-auto md:min-h-screen pt-16 pb-8 flex flex-col bg-meet-team bg-center ">
                <div className="relative w-full ">
                    <div >
                        <span className="uppercase text-41 sm:text-lg md:text-2xl  leading-5 font-bold text-white ">
                            MEET THE TEAM
                        </span>
                    </div>

                    <div

                        className="container lg:max-w-screen-xl mx-auto flex flex-wrap  justify-center pt-32  md:pt-44"
                    >
                        <div>
                            <TeamMemberCard url="1127.png" name=" John" postion="(Co-Founder)">
                                Visionary with the minds of ethereum blockchain as the future goal of currency
                            </TeamMemberCard>

                        </div>
                        <div>
                            <TeamMemberCard url="1127.png" name="Daniel" postion="(Artist)">
                                Visionary with the minds of ethereum blockchain as the future goal of currency
                            </TeamMemberCard>

                        </div>
                        <div>
                            <TeamMemberCard url="1127.png" name="Mathew" postion="(Community Manager)">
                                Visionary with the minds of ethereum blockchain as the future goal of currency
                            </TeamMemberCard>

                        </div>
                        <div>
                            <TeamMemberCard url="1127.png" name="Daniel" postion="(Artist)">
                                Visionary with the minds of ethereum blockchain as the future goal of currency
                            </TeamMemberCard>

                        </div>
                    </div>

                </div>
            </section>

        </>
    )
}
