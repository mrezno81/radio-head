import React from 'react'

export default function Footer() {
    return (
        <div>
            <div className=" bg-drakblue pt-2">
                <div className="flex py-20 px-20 m-auto border-t text-gray-800 text-sm flex-col
      max-w-screen-lg items-center">
                    <img className=" w-40" alt='2565' src="/assets/TVHG.LOGO.INV2.png" />
                    <span className="mt-2 text-[13px] text-center font-thin text-white" >
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                    </span>
                    <div className="my-2 text-white font-normal">© Copyright 2020. All Rights Reserved.</div>

                </div>
            </div>



        </div>
    )
}
