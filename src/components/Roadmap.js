import React from 'react'

const RoadMapCard = ({ stage, date, month, children, className }) => {
    return (
        <>
            <section className='flex items-center my-10'>
                <div className='flex justify-center items-center'>
                    <div style={{
                        height: "175px",
                        width: "181px",
                        border: "3px solid #7ED3ED",
                        borderRadius: "19px", opacity: "1",
                        color: "white",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: "column"

                    }}>
                        <span className=''>{stage}</span> <br />
                        <span>{date}</span><br />
                        <span>{month}</span>

                    </div>
                    <div style={{
                        background: "#7ED3ED",
                        height: "3px",
                        width: "50px",
                        opacity: "1"
                    }}></div>
                    <div style={{
                        height: "15px",
                        width: "15px",
                        background: "#7ED3ED 0% 0% no-repeat padding-box",
                        borderRadius: "100%",
                        opacity: "1"
                    }}></div>
                </div>
                <div className='w-80 px-5'>
                    <span className="mt-2 text-[13px] text-center font-thin text-white" >
                        {children}
                    </span>
                </div>



            </section>
        </>
    )
}

export default function Roadmap() {
    return (
        <div>
            <section className="relative mx-auto md:min-h-screen pt-16 pb-8 flex flex-col bg-road-map bg-center ">
                <div className="relative w-full ">
                    <div >
                        <span className="uppercase text-41 sm:text-lg md:text-2xl  leading-5 font-bold text-white ">
                            MEET THE TEAM
                        </span>
                    </div>
                </div>
                <div

                    className="container lg:max-w-screen-xl mx-auto flex overflow-auto md:flex-wrap justify-start md:justify-center pt-32  md:pt-44"
                >
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>
                    <div className='w-2/4'>
                        <RoadMapCard stage="1st Stage" date="2021 Q4 " month="October" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        </RoadMapCard>

                    </div>


                </div>


            </section>
        </div >
    )
}
