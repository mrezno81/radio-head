import React, { useState } from 'react'
import questionsAnswers from "./questionData"
import './faq.css'
const FaqItem = ({
    showDescription,
    ariaExpanded,
    fontWeightBold,
    item,
    index,
    onClick,
}) => (
    <div className="faq__question" key={item.question}>
        <dt>
            <button
                aria-expanded={ariaExpanded}
                aria-controls={`faq${index + 1}_desc`}
                className={`faq__question-button ${fontWeightBold} text-white`}
                onClick={onClick}
            >
                {item.question}
            </button>
        </dt>
        <dd>
            <p
                id={`faq${index + 1}_desc`}
                data-qa="faq__desc"
                className={`faq__desc ${showDescription}`}
            >
                {item.answer}
            </p>
        </dd>
    </div>
);




export default function FaqSection() {
    const [activeIndex, setActiveIndex] = useState(1);

    const renderedQuestionsAnswers = questionsAnswers.map((item, index) => {
        const showDescription = index === activeIndex ? "show-description" : "";
        const fontWeightBold = index === activeIndex ? "font-weight-bold" : "";
        const ariaExpanded = index === activeIndex ? "true" : "false";
        return (
            <FaqItem
                key={item.question}
                showDescription={showDescription}
                fontWeightBold={fontWeightBold}
                ariaExpanded={ariaExpanded}
                item={item}
                index={index}
                onClick={() => {
                    setActiveIndex(index);
                }}
            />
        );
    });

    return (
        <div className="faq bg-drakblue" >
            <h1 className="faq__title text-white">FAQ</h1>
            <dl className="faq__list text-white">{renderedQuestionsAnswers}</dl>
        </div>
    );
};

