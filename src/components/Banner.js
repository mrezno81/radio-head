import React from 'react'

export default function Banner() {
    return (
        <>
            <div className="md:min-w-screen relative">
                <div className="lg:pt-0 md:left-0 w-full ">
                    <img className=" -z-50 object-cover" alt='background' src="/assets/Mask Group 3.png" />
                </div>
                <section className="sm:static mx-auto flex justify-center items-end">
                    <div className="flex items-center  absolute bottom-10 flex-col z-50 max-w-xs sm:max-w-none ">
                        <img className=" z-50  w-6/12  " alt='2564' src="/assets/Group 2564.png" />
                        <img className=" z-50 w-6/12 " alt='2565' src="/assets/Group 2565.png" />

                    </div>
                </section>

            </div>

        </>
    )
}
