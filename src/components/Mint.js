import React, { useState } from 'react'
import "./Mint.css"

export default function Mint() {
    const [value, setValue] = useState(1);

    const handleChanges = e => {

        setValue(e.target.value.replace(/\D/, ""));


    };

    const onUpArrow = () => {
        setValue(value => value + 1);
        console.log(value)

    }
    const onDownArrow = () => {
        setValue(value => value - 1);
    }
    return (
        <>
            <section className="w-full pt-16 pb-16 flex flex-col bg-center bg-purple ">
                <div className="w-full px-24 wrapper" >
                    <div className='mb-9'>
                        <img className="object-contain object-right m-auto" src={`/assets/4.png`} alt="avatar" width="80%" height="80%%" />

                    </div>
                    <div className=' w-2/4 flex flex-col justify-center items-center' >
                        <span className="pb-8 uppercase text-41 sm:text-lg md:text-2xl  leading-5 font-bold text-white ">
                            MINT A HEAD
                        </span>
                        <h3 className=" font-normal text-lg mb-1 text-white">0 / 5000 Miner at 0.04 ETH each</h3>
                        <span className="mt-2 text-[13px] text-center font-thin text-white" >
                            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                        </span>
                        <div className="flex flex-wrap mt-6">
                            <div className='w-6/12 flex flex-wrap'>
                                <div className="flex w-10/12">
                                    <input type="text" pattern="[0-9]*" value={value} onChange={e => handleChanges(e)}
                                        className="bg-white border-none text-sm text-gray-900 text-center focus:outline-none border border-gray-800 focus:border-gray-600 rounded-l-md w-full" />
                                </div>
                                <div className="flex flex-col w-2/12">
                                    <button onClick={onUpArrow} disabled={value === 10 ? true : ""}
                                        className="text-drakblue border-none text-center text-md font-semibold rounded-tr-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none  bg-white">
                                        ↑
                                    </button>
                                    <button onClick={onDownArrow} disabled={value === 0 ? true : ""}
                                        className="text-drakblue border-none text-center text-md font-semibold rounded-br-md px-1 bg-gray-800 focus:bg-gray-600 focus:outline-none  bg-white">
                                        ↓
                                    </button>
                                </div>
                            </div>
                            <div className='w-6/12'>
                                <button className="bg-light-purple hover:bg-blue-700 text-white font-bold py-2 px-4 h-12 w-40 ml-3 rounded-3xl">
                                    MINT
                                </button>
                            </div>

                        </div>
                    </div>

                    <div className='mt-9' >
                        <img className="object-contain object-left m-auto " src={`/assets/5.png`} alt="avatar" width="80%" height="80%" />

                    </div>
                </div>
            </section>

        </>
    )
}
