import React from 'react'
import ReactPlayer from "react-player";


const VideoScreen = ({ url, image, className }) => {
    return (

        <>
            <ReactPlayer
                url={`/assets/trailer/${url}`}
                width="100%"
                height="520px"
                playing
                light={`/assets/trailer/${image}`}
                className={`object-contain  ${className} `}
            />
        </>
    )
}

export default function Trailer() {

    return (
        <>
            <section className="mx-auto md:min-h-screen pt-16 pb-8 flex flex-col bg-center bg-purple ">
                <div className="w-full ">
                    <div >
                        <span className="pb-8 uppercase text-41 sm:text-lg md:text-2xl  leading-5 font-bold text-white ">
                            Trailer
                        </span>
                    </div>
                    <div className='flex justify-center'>
                        <div className='pt-10 h-screen w-full  ' style={{ width: "75%", height: "520px" }}>
                            <VideoScreen url="Htgn2KJXASjwOYWi.mp4" image="Htgn2KJXASjwOYWi_PosterImage.png"/>

                        </div>

                    </div>

                </div>
            </section>

        </>
    )
}
